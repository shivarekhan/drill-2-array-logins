const inputarr = [{ "id": 1, "first_name": "Valera", "last_name": "Pinsent", "email": "vpinsent0@google.co.jp", "gender": "Male", "ip_address": "253.171.63.171" },
{ "id": 2, "first_name": "Kenneth", "last_name": "Hinemoor", "email": "khinemoor1@yellowbook.com", "gender": "Polygender", "ip_address": "50.231.58.150" },
{ "id": 3, "first_name": "Roman", "last_name": "Sedcole", "email": "rsedcole2@addtoany.com", "gender": "Genderqueer", "ip_address": "236.52.184.83" },
{ "id": 4, "first_name": "Lind", "last_name": "Ladyman", "email": "lladyman3@wordpress.org", "gender": "Male", "ip_address": "118.12.213.144" },
{ "id": 5, "first_name": "Jocelyne", "last_name": "Casse", "email": "jcasse4@ehow.com", "gender": "Agender", "ip_address": "176.202.254.113" },
{ "id": 6, "first_name": "Stafford", "last_name": "Dandy", "email": "sdandy5@exblog.jp", "gender": "Female", "ip_address": "111.139.161.143" },
{ "id": 7, "first_name": "Jeramey", "last_name": "Sweetsur", "email": "jsweetsur6@youtube.com", "gender": "Genderqueer", "ip_address": "196.247.246.106" },
{ "id": 8, "first_name": "Anna-diane", "last_name": "Wingar", "email": "awingar7@auda.org.au", "gender": "Agender", "ip_address": "148.229.65.98" },
{ "id": 9, "first_name": "Cherianne", "last_name": "Rantoul", "email": "crantoul8@craigslist.org", "gender": "Genderfluid", "ip_address": "141.40.134.234" },
{ "id": 10, "first_name": "Nico", "last_name": "Dunstall", "email": "ndunstall9@technorati.com", "gender": "Female", "ip_address": "37.12.213.144" }]

/* 

    1. Find all people who are Agender
    2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    3. Find the sum of all the second components of the ip addresses.
    3. Find the sum of all the fourth components of the ip addresses.
    4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    5. Filter out all the .org emails
    6. Calculate how many .org, .au, .com emails are there
    7. Sort the data in descending order of first name

    NOTE: Do not change the name of this file

*/

function agenderFilter() {
    let result = inputarr.filter((inputObject) => {
        if (inputObject.gender.includes("Agender") == true) {
            return inputObject;
        }
    })
    return result;
}

function transformIpAddress(inputarr) {
    let result = inputarr.map((inputObject) => {
        return inputObject.ip_address.split(".");
    })
    return result;
}

function sumOfIpAddressSeond(inputarr) {
    let auxResult = transformIpAddress(inputarr);
    console.log(auxResult);
    let result = auxResult.reduce((acc, current) => {
        acc = Number(acc) + Number(current[1]);
        return acc;
    }, []);
    return result;
}

function sumOfIpAddressFourth(inputarr) {
    let auxResult = transformIpAddress(inputarr);
    console.log(auxResult);
    let result = auxResult.reduce((acc, current) => {
        acc = Number(acc) + Number(current[3]);
        return acc;
    }, []);
    return result;
}

function getFUllName(inputarr) {
    let result = inputarr.map((currentObject) => {
        currentObject["full_name"] = currentObject.first_name + " " + currentObject.last_name;
        return currentObject;
    })
    return result;
}

function mailFilterOrg(inputarr) {
    let result = inputarr.filter((currentObject) => {
        if (currentObject.email.endsWith(".org") == true) {
            return currentObject;
        }
    })
    return result;
}

function calculateHowManyMails(inputarr) {
    let result = inputarr.reduce((acc, currentObject) => {
        if (currentObject.email.endsWith(".org")) {
            acc[".org"]++;
        }
        else if (currentObject.email.endsWith(".au")) {
            acc[".au"]++;
        }
        else if (currentObject.email.endsWith(".com")) {
            acc[".com"]++;
        }
        return acc;
    }, {
        ".org": 0,
        ".com": 0,
        ".au": 0,
    })
    return result;
}

function sortDecending(inputarr) {
    let result = inputarr.map(currentObject => currentObject)
        .sort((object1, object2) =>
            object2.first_name.localeCompare(object1.first_name));

    return result;
}

console.log(calculateHowManyMails(inputarr));
